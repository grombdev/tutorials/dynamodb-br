from scripts import add_inverted_index, add_secondary_index, bulk_load_table, create_table, delete_table
from application import fetch_game_and_players, find_games_for_user, find_open_games_by_map, find_open_games, join_game, start_game

def main():
    create_table.create_table()
    bulk_load_table.bulk_load_table()
    fetch_game_and_players.fetch_game_and_players()

    add_secondary_index.add_secondary_index()
    find_open_games_by_map.find_open_games_by_map()
    find_open_games.find_open_games()
    join_game.join_game()
    start_game.start_game()
    add_inverted_index.add_inverted_index()
    find_games_for_user.find_games_for_user()

    delete_table.delete_table()

    print("Exiting...")

if __name__ == "__main__":
    main()
import json
import boto3
from scripts import dynamodb_cfg

# to check than it works: `aws dynamodb scan --table-name battle-royale --select COUNT`
def bulk_load_table():
    dynamodb = dynamodb_cfg.GetDynamoDBResource()
    table = dynamodb.Table('battle-royale')

    items = []

    with open('/code/scripts/items.json', 'r') as f:
        for row in f:
            items.append(json.loads(row))

    with table.batch_writer() as batch:
        for item in items:
            batch.put_item(Item=item)

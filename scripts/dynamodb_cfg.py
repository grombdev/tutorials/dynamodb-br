import boto3

def GetDynamoDBClient():
    #dynamodb = boto3.resource('dynamodb', endpoint_url='http://localhost:8000', region_name='eu-west-3')
    dynamodb = boto3.client('dynamodb', endpoint_url='http://dynamodb:8000', region_name='eu-west-3')
    return dynamodb


def GetDynamoDBResource():
    dynamodb = boto3.resource('dynamodb', endpoint_url='http://dynamodb:8000', region_name='eu-west-3')
    return dynamodb

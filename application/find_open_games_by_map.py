import boto3
from application.entities import Game
from scripts import dynamodb_cfg

def __find_open_games_by_map(map_name):
    dynamodb = dynamodb_cfg.GetDynamoDBClient()
    resp = dynamodb.query(
        TableName='battle-royale',
        IndexName="OpenGamesIndex",
        KeyConditionExpression="#map = :map",
        ExpressionAttributeNames={
            "#map": "map"
        },
        ExpressionAttributeValues={
            ":map": { "S": map_name },
        },
        ScanIndexForward=True
    )

    games = [Game(item) for item in resp['Items']]

    return games

def find_open_games_by_map():
    MAP_NAME = "Green Grasslands"
    games = __find_open_games_by_map(MAP_NAME)
    print("Open games for {}:".format(MAP_NAME))
    for game in games:
        print(game)

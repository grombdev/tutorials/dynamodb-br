import boto3
from application.entities import Game
from scripts import dynamodb_cfg

def __find_open_games():
    dynamodb = dynamodb_cfg.GetDynamoDBClient()
    resp = dynamodb.scan(
        TableName='battle-royale',
        IndexName="OpenGamesIndex",
    )

    games = [Game(item) for item in resp['Items']]

    return games

def find_open_games():
    games = __find_open_games()
    print("Open games:")
    for game in games:
        print(game)
